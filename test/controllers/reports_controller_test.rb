require 'test_helper'

class ReportsControllerTest < ActionDispatch::IntegrationTest
  test "should get demo" do
    get reports_demo_url
    assert_response :success
  end

  test "should get index" do
    get reports_index_url
    assert_response :success
  end

end
