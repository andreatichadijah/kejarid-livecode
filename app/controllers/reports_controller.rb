class ReportsController < ApplicationController
  def new #untuk menampilkan form data baru
    @report = Report.new
  end

  def create #untuk memproses data baru yang dimasukkan di form new
    report = Report.new(resource_params)
    report.save
    flash[:notice] = 'Report has been created'
    redirect_to reports_path 
  end

  def edit #menampilkan data yang sudah disimpan diedit
    id = params[:id]
    @reports = Report.find(id)
    
  end

  def update #melakukan proses ketika user mengedit data
    id = params[:id]
    @report = Report.find(params[:id])
    @report.update(resource_params)
    flash[:notice] = 'Report has been update'
    redirect_to reports_path(@report)
  end

  def destroy #untuk menghapus data
    id = params[:id]
    @report = Report.find(params[:id])
    @report.destroy
    flash[:notice] = 'Report has been deleted'
    redirect_to reports_path(@report)
  end

  def index #menampilkan seluruh data yang ada di database
    @reports = Report.all
  end

  def show #menampilkan sebuah data secara detail
    id = params[:id]
    @report = Report.find(id)
    # render plain:id
    # render plain: @book.title
  end

  def resource_params
    params.require(:report).permit(:title, :hasil, :mapel, :teacher_id, :student_id, :date)
  end
end
