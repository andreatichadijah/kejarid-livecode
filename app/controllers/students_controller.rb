class StudentsController < ApplicationController
  def new #untuk menampilkan form data baru
    @student = Student.new
  end

  def create #untuk memproses data baru yang dimasukkan di form new
    student = Student.new(resource_params)
    student.save
    flash[:notice] = 'Student has been created'
    redirect_to student_path 
  end

  def edit #menampilkan data yang sudah disimpan diedit
    id = params[:id]
    @students = Student.find(id)
    
  end

  def update #melakukan proses ketika user mengedit data
    id = params[:id]
    @student = Student.find(params[:id])
    @student.update(resource_params)
    flash[:notice] = 'Student has been update'
    redirect_to students_path(@student)
  end

  def destroy #untuk menghapus data
    id = params[:id]
    @student = Student.find(params[:id])
    @student.destroy
    flash[:notice] = 'Student has been deleted'
    redirect_to students_path(@student)
  end

  def index #menampilkan seluruh data yang ada di database
    @students = Student.all
  end

  def show #menampilkan sebuah data secara detail
    id = params[:id]
    @student = Student.find(id)
    # render plain:id
    # render plain: @book.title
  end

  def resource_params
    params.require(:student).permit(:name, :username, :age, :kelas, :address, :city, :nik)
  end
end
