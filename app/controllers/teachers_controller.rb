class TeachersController < ApplicationController
  def new #untuk menampilkan form data baru
    @teacher = Teacher.new
  end

  def create #untuk memproses data baru yang dimasukkan di form new
    teacher = Teacher.new(resource_params)
    teacher.save
    flash[:notice] = 'Teacher has been created'
    redirect_to teachers_path 
  end

  def edit #menampilkan data yang sudah disimpan diedit
    id = params[:id]
    @teachers = Teacher.find(id)
    
  end

  def update #melakukan proses ketika user mengedit data
    id = params[:id]
    @teacher = Teacher.find(params[:id])
    @teacher.update(resource_params)
    flash[:notice] = 'Teacher has been update'
    redirect_to teachers_path(@teacher)
  end

  def destroy #untuk menghapus data
    id = params[:id]
    @teacher = Teacher.find(params[:id])
    @teacher.destroy
    flash[:notice] = 'Teacher has been deleted'
    redirect_to teachers_path(@teacher)
  end

  def index #menampilkan seluruh data yang ada di database
    @teachers = Teacher.all
  end

  def show #menampilkan sebuah data secara detail
    id = params[:id]
    @teacher = Teacher.find(id)
    # render plain:id
    # render plain: @book.title
  end

  def resource_params
    params.require(:teacher).permit(:nik, :name, :age, :kelas, :mapel)
  end
end
