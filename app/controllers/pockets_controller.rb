class PocketsController < ApplicationController
  def new #untuk menampilkan form data baru
    @pocket = Pocket.new
  end

  def create #untuk memproses data baru yang dimasukkan di form new
    pocket = Pocket.new(resource_params)
    pocket.save
    flash[:notice] = 'Pocket has been created'
    redirect_to pockets_path 
  end

  def edit #menampilkan data yang sudah disimpan diedit
    id = params[:id]
    @pockets = Pocket.find(id)
    
  end

  def update #melakukan proses ketika user mengedit data
    id = params[:id]
    @pocket = Pocket.find(params[:id])
    @pocket.update(resource_params)
    flash[:notice] = 'Pocket has been update'
    redirect_to pockets_path(@pocket)
  end

  def destroy #untuk menghapus data
    id = params[:id]
    @pocket = Pocket.find(params[:id])
    @pocket.destroy
    flash[:notice] = 'Teacher has been deleted'
    redirect_to pockets_path(@pocket)
  end

  def index #menampilkan seluruh data yang ada di database
    @pockets = Pocket.all
  end

  def show #menampilkan sebuah data secara detail
    id = params[:id]
    @pocket = Pocket.find(id)
    # render plain:id
    # render plain: @book.title
  end

  def resource_params
    params.require(:pocket).permit(:balance, :student_id, :teacher_id)
  end
end
