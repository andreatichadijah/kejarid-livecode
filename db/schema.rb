# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_09_092512) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "exams", force: :cascade do |t|
    t.string "title", limit: 50, null: false
    t.string "mapel", limit: 50, null: false
    t.integer "duration", default: 0
    t.float "nilai", default: 0.0
    t.string "status", null: false
    t.integer "level", null: false
    t.string "student_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "payments", force: :cascade do |t|
    t.string "id_transaction"
    t.string "status", null: false
    t.text "upload", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "pockets", force: :cascade do |t|
    t.float "balance", null: false
    t.string "student_id"
    t.string "teacher_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "reports", force: :cascade do |t|
    t.string "title", null: false
    t.float "hasil", null: false
    t.string "mapel", null: false
    t.string "teacher_id", null: false
    t.string "student_id", null: false
    t.datetime "date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "students", force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.string "username", limit: 50, null: false
    t.integer "age", null: false
    t.string "kelas", null: false
    t.string "address"
    t.string "city", null: false
    t.string "nik", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "teachers", force: :cascade do |t|
    t.string "nik", null: false
    t.string "name", limit: 50, null: false
    t.integer "age", null: false
    t.string "kelas", null: false
    t.string "mapel", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

end
