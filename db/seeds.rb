# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Student.create(name: 'Chadijah Andreati P', username: 'andreatiputri', age: 22, kelas: 'kelas 12', address: 'Perumhanan PJKA', city: 'Kabupaten Bogor', nik: '11303408')
Exam.create(title: 'Latihan 1', name: 'Chadijah Andreati P', duration: '30 menit', nilai: 100, status: 'Aktif', level: 2)
Teacher.create(nik: '10102012', name: 'Dina', age: 34, kelas: 'kelas 12', mapel: 'Bahasa Indonesia')
Report.create(title: 'Latihan 1', hasil: 100, mapel: 'Bahasa Indonesia', date: '2020-02-08')