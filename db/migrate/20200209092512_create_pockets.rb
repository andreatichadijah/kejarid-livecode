class CreatePockets < ActiveRecord::Migration[6.0]
  def change
    create_table :pockets do |t|
      t.float :balance, null: false
      t.string :student_id
      t.string :teacher_id

      t.timestamps
    end
  end
end
