class CreatePayments < ActiveRecord::Migration[6.0]
  def change
    create_table :payments do |t|
      t.string :id_transaction
      t.string :status, null: false
      t.text :upload, null: false

      t.timestamps
    end
  end
end
