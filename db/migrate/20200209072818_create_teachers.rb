class CreateTeachers < ActiveRecord::Migration[6.0]
  def up
    create_table :teachers do |t|
      t.string :nik, null: false
      t.string :name, null: false, limit: 50
      t.integer :age, null: false
      t.string :kelas, null: false
      t.string :mapel, null: false

      t.timestamps
    end
  end
  def down
    drop_table = teachers
  end
end
