Rails.application.routes.draw do
  # get 'pockets/demo'
  # get 'pockets/index'
  # get 'payments/demo'
  # get 'payments/index'
  # get 'reports/demo'
  # get 'reports/index'
  # get 'exams/demo'
  # get 'exams/index'
  # get 'teachers/demo'
  # get 'teachers/index'
  # get 'students/demo'
  # get 'students/index'

  resources :students
  resources :teachers
  resources :exams
  resources :reports
  resources :pockets
  resources :payments
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
